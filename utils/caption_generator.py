from keras.utils import pad_sequences
import numpy as np
from .preprocessing import preprocess_image,idx_to_word
from .model_loader import load_model_from_directory, load_mapping, load_tokenizer,load_features

def generate_caption(model,mapping,features,tokenizer,image_name):
    image_id = image_name.split('.')[0]
    img_path = preprocess_image(image_name)
    captions = mapping[image_id]
    captions = [caption.replace('startseq', '').replace('endseq', '').replace("'", '') for caption in captions]
    max_length = 35
    y_pred = predict_caption(model, features[image_id], tokenizer, max_length).replace('startseq', '').replace('endseq', '').replace("'", '')
    return y_pred, captions

def predict_caption(model, image, tokenizer, max_length):
    in_text = 'startseq'
    for i in range(max_length):
        sequence = tokenizer.texts_to_sequences([in_text])[0]
        sequence = pad_sequences([sequence], max_length)
        yhat = model.predict([image, sequence], verbose=0)
        yhat = np.argmax(yhat)
        word = idx_to_word(yhat, tokenizer)
        if word is None:
            break
        in_text += " " + word
        if word == 'endseq':
            break
    return in_text