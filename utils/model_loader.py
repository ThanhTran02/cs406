import os
from keras.models import load_model
import pickle
from keras.preprocessing.text import Tokenizer

def load_model_from_directory():
    return load_model('./model/best_model5.h5')

def load_mapping():
    mapping_file_path = './model/mapping.txt'
    mapping = {}
    with open(mapping_file_path, 'r') as txt_file:
        for line in txt_file:
            if ':' in line:
                image_id, caption_str = line.strip().split(':', 1)
                captions = [caption.strip() for caption in caption_str.split(',')]
                mapping[image_id] = captions
    return mapping

def load_features():
    tokenizer_file_path = './model/'
    with open(os.path.join(tokenizer_file_path, 'features5.pkl'), 'rb') as f:
        return pickle.load(f)
    
def load_tokenizer(mapping):
    all_captions = []
    for key in mapping:
        for caption in mapping[key]:
            all_captions.append(caption)
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(all_captions)
    return tokenizer