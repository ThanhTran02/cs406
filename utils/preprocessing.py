from PIL import Image
import os

BASE_DIR = "static/"

def preprocess_image(image_name):
    image_path = os.path.join(BASE_DIR, "Images", image_name)
    image = Image.open(image_path)
    return image_path

def idx_to_word(integer, tokenizer):
    for word, index in tokenizer.word_index.items():
        if index == integer:
            return word
    return None