
from flask import Flask, request, render_template
from utils import generate_caption,load_model_from_directory, load_mapping, load_tokenizer,load_features
import os

UPLOAD_FOLDER = 'static/uploads'
ALLOWED_EXTENSIONS = {'jpg', 'jpeg', 'png', 'gif'}

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

model = load_model_from_directory()
mapping = load_mapping()
features = load_features()
tokenizer = load_tokenizer(mapping)

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if 'image' not in request.files:
            return render_template('index.html', error='No image file selected')

        file = request.files['image']

        if file.filename == '':
            return render_template('index.html', error='No image file selected')

        if file and allowed_file(file.filename):
            if not os.path.exists(app.config['UPLOAD_FOLDER']):
                os.makedirs(app.config['UPLOAD_FOLDER'])

            image_path = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
            file.save(image_path)

            predictions, captions = generate_caption(model,mapping,features,tokenizer,file.filename)
            return render_template('index.html', predictions=predictions, image_path=file.filename, captions=captions)
        else:
            return render_template('index.html', error='Invalid file format. Only JPG, JPEG, and PNG files are supported.')

    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
